<?php get_header(); ?>

<section class="main-content">
  <div class="container">
    <div class="title">
      <h3>ACFID - STATE OF THE SECTOR REPORT 2018</h3>
    </div>
    <div class="content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit dui vel sapien elementum, at rutrum orci interdum curabitur eget augue ut nunc tristique accumsan. Curabitur urna tortor, feugiat nec blandit quis, convallis eget risus. Fusce ut arcu massa. Vestibulum eleifend, odio vitae semper suscipit, lectus purus ultrices est, ac viverra augue quam vel diam. Praesent rutrum</p> 
    </div>
    <section class="home-menu">
      <div class="container">
        <div class="column-wrapper justify-content-around align-items-center text-center">
          <div class="menu-item">
            <a href="#"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon1.png" class="img-fluid"><p>HIGHLIGHTS</p></a>
          </div>
          <div class="menu-item">
            <a href="#"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon2.png" class="img-fluid"><p>DEVELOPMENT NGO'S</p></a>
          </div>
          <div class="menu-item">
            <a href="#"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon3.png" class="img-fluid"><p>TOTAL REVENUE</p></a>
          </div>
        </div>
        <div class="column-wrapper justify-content-around align-items-center text-center">
          <div class="menu-item">
            <a href="#"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon4.png" class="img-fluid"><p>AUSTRALIAN DEVELOPMENT NGO'S</p></a>
          </div>
          <div class="menu-item">
            <a href="#"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon5.png" class="img-fluid"><p>TRENDS IN DONATIONS</p></a>
          </div>
          <div class="menu-item">
            <a href="#"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon6.png" class="img-fluid"><p>GENDER EQUALITY</p></a>
          </div>
        </div>
      </div>
    </section>
    <div class="button-wrap">
      <a href="#" class="btn custom-btn"><i class="far fa-file-pdf"></i>DOWNLOAD THE FULL REPORT</a>
    </div>
    </div>
</section>


<?php get_footer(); ?>