<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="color" content="#006cff">
    <!-- <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.css"> -->
    <title>ACFID</title>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?> >
  <nav class="navbar main navbar-expand-lg navbar-light">
  <div class="container-fluid">
  <div class="logo-wrap">
    <a class="navbar-brand" href="<?php echo get_site_url();?>">
      <img src="<?php echo get_template_directory_uri();?>/assets/images/logo.png" alt="logo" class="img-fluid">
      <!-- <i class="fab fa-fort-awesome-alt"></i> -->
    </a>
  </div>
  <div class="toggle-wrap hide-button">
    <button style="display:block;" class="navbar-toggler"  id="sidebarCollapse">
      <span></span>
      <span></span>
      <span></span>
    </button>
    <span>ACFID - STATE OF THE SECTOR REPORT 2018</span>
  </div>

<!--
  <div class="main-nav-wrapper collapse navbar-collapse" id="main-nav">
    <ul class="navbar-nav">
      <li class="nav-item active">

      </li>
      <!-- <li class="dropdown nav-item">
        <a class="nav-link" href="#">Example</a>
        <button class="dropdown-toggle" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent"
          aria-expanded="false" aria-label="Toggle navigation">
          <i class="fa fa-chevron-down"></i>
        </button>
        <div class="dropdown-collapse collapse" id="navbarToggleExternalContent">
          <div class="bg-theme-dark py-4">
          <a class="dropdown-item" href="landing-page.php" target="_blank">Example landing page</a>
          </div>
        </div>
      </li> -->
      <!-- <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact</a>
      </li> 
    </ul>
    <div class="searchMainNav" id="searchMainNav">
        <div class="search-main-wrapper">
          <form action="">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search">
              <button type="submit"><i class="fa fa-search"></i></button>
            </div>
          </form>
        </div>
      </div>
  </div>
  -->
   <div class="right-side-icons">
      <div class="searchMainNav wide" id="searchMainNav">
        <div class="search-main-wrapper">
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
  </div>
</nav>
<?php get_sidebar(); ?>

