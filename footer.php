    <div id="sidebar2">
      <div class="list-wrap">
        <ul class="list-unstyled">
          <li class="pdf-file"><a href="<?php the_field('upload_file_report','options'); ?>"><i class="far fa-file-pdf"></i>Download the full report</a></li>
       <li class="abbr"><i class="fa fa-question-circle" aria-hidden="true"></i> Abbrevations</li>
        </ul>
        <div id="popupBox">
          <div class="pop-title">
            <h5>List of abbreviations used<img class="img-fluid pop-close" src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png"></h5>
          </div>
          <div class="pop-content">
            <?php the_field('content_pop','options'); ?>
          </div>
        </div>
      </div>
     </div>
    <footer class="footer">
      <div class="container-fluid">
        <div class="footer-wrapper">
          <p>Copyright 2018 ACFID</p>
          <p>Website by <a href="https://www.oskyinteractive.com.au/">OSKY</a></p>
        </div>
      </div>
    </footer>
    </div>
  </div>
   </div>
  <!-- End Page Content -->
    <?php wp_footer(); ?>
  </body>
</html>