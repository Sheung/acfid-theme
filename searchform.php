<?php
/**
 * default search form
 */
?>
<form role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="form-group">
    	<div class="input-wrapper">
    	<input type="search" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" class="form-control" placeholder="Search">
        <button type="submit" type="submit" id="search-submit" value="Search"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>