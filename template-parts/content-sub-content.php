<section class="main-content">
<div class="subpage">
  <div class="container">
    <div class="title">
      <h4><?php the_title(); ?></h4>
    </div>
    <div class="content content-fade">
      <?php the_content(); ?>
      <div class="button-wrap button-wrap-fade text-center">
        <a href="#" class="btn custom-btn-gradient">READ MORE</a>
      </div>
    </div>
  </div>
</div>
<?php
$i = 1;
// check if the repeater field has rows of data
if( have_rows('accordian_repeat') ):

  // loop through the rows of data
    while ( have_rows('accordian_repeat') ) : the_row();
?>
<div id="accordtitle<?php echo $i ?>" class="accord accord-sec<?php echo $i ?>">
  <div class="container">
    <div class="accord-head-title" onclick="jQuery('#accordinner1').click()">
      <a id="accord-link" class="accord-link" href="#accordtitle<?php echo $i ?>"><h4 class="accord-title<?php echo $i ?> accordian"><?php the_sub_field('title_accord') ?></h4></a>
    </div>
    <div class="accord-head-button">
       <a class="accord-btn show-all-applying"><img class="show-img<?php echo $i ?> img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/plus.png"></a>
    </div>
  </div>
</div>
<div class="faq-accordion applying">
  <div class="accord-content container">
    <div class="accord-wrap"></div>
    <div id="accordtitle<?php echo $i ?>" class="accord-details<?php echo $i ?>">
        <?php the_sub_field('content_accord') ?>
    </div>
  </div>
</div>
<?php
    $i++;
    endwhile;
else:

  // no rows found

endif;

?>
<?php if( get_field('next_chapter') ): ?>
<div class=" btn-chapter-wrap">
    <a href="<?php the_field('next_chapter'); ?>" class="btn btn-chapter custom-btn">NEXT CHAPTER<i class="fa fa-arrow-right"></i></a>
</div>
<?php endif; ?>

</section>