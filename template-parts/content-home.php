<section class="main-content">
  <div class="container">
    <div class="title">
      <h3><?php the_title(); ?></h3>
    </div>
    <div class="content">
      <?php the_content();?>
    </div>
    <section class="home-menu">
      <div class="container">
        <div class="column-wrapper justify-content-around align-items-center text-center">
          <?php
          // check if the repeater field has rows of data
          if( have_rows('cta_home') ):

          // loop through the rows of data
          while ( have_rows('cta_home') ) : the_row(); ?>

          <div class="menu-item">
            <a href="<?php the_sub_field('url_cta_home'); ?>"><img src="<?php the_sub_field('image_cta_home'); ?>" class="img-fluid"><p><?php the_sub_field('title_cta_home'); ?></p></a>
          </div>

          <?php

          endwhile;

          else :

          // no rows found

           endif;
          ?>
        </div>
    </section>
    <div class="button-wrap">
      <a href="<?php the_field('upload_file_report','options'); ?>" class="btn custom-btn"><i class="far fa-file-pdf"></i>DOWNLOAD THE FULL REPORT</a>
    </div>
    </div>
</section>