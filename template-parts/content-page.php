<section class="main-content default">
<div class="subpage default">
  <div class="container">
    <div class="title">
      <h4><?php the_title(); ?></h4>
    </div>
    <div class="content">
      <?php the_content(); ?>
      <!--
      <div class="button-wrap text-center">
        <a href="#" class="btn custom-btn-gradient">READ MORE</a>
      </div>
      -->
    </div>
  </div>
</div>
<?php
$i = 1;
// check if the repeater field has rows of data
if( have_rows('accordian_repeat') ):

  // loop through the rows of data
    while ( have_rows('accordian_repeat') ) : the_row();
?>
<div class="accord">
  <div class="container">
    <a class="accord-btn show-all-applying"><img class="show-img<?php echo $i ?> img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/plus.png"></a>
    <div class="faq-accordion applying">
      <div class="accord-content">
        <h4><?php the_sub_field('title_accord') ?></h4>
        <div class="accord-details<?php echo $i ?>">
            <?php the_sub_field('content_accord') ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
    $i++;
    endwhile;
else:

  // no rows found

endif;

?>
<?php if( get_field('next_chapter') ): ?>
<div class="button-wrap btn-chapter-wrap">
    <a href="<?php the_field('next_chapter'); ?>" class="btn btn-chapter custom-btn">NEXT CHAPTER<i class="fa fa-arrow-right"></i></a>
</div>
<?php endif; ?>
</section>